/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package uk.jeesoft.screenmedia;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Date;
import java.util.Map;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.BDDAssertions.then;
import static org.junit.Assert.assertNotNull;
import org.junit.ComparisonFailure;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import uk.jeesoft.screenmedia.domain.CalcParams;
import uk.jeesoft.screenmedia.domain.Calculation;
import uk.jeesoft.screenmedia.types.FuelType;
import uk.jeesoft.screenmedia.types.Service;

/**
 * Basic integration tests for service demo application.
 *
 * @author Dave Syer
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(properties = {"management.port=0"})
public class CalculatorApplicationTests {

    private static Logger logger = LoggerFactory.getLogger(CalculatorApplicationTests.class);

    @LocalServerPort
    private int port;

    @Value("${local.management.port}")
    private int mgt;

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldReturn200WhenSendingRequestToController() throws Exception {

        String url = "http://localhost:" + this.port + Service.calculator + "/hello-world";
        ResponseEntity<Map> entity = this.testRestTemplate.getForEntity(url, Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test
    public void shouldReturn200WhenSendingRequestToManagementEndpoint() throws Exception {

        ResponseEntity<Map> entity = this.testRestTemplate.getForEntity(
                "http://localhost:" + this.mgt + "/actuator/info", Map.class);

        then(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    @Test(expected = ComparisonFailure.class) // Until classes are prperly implemented
    public void shouldReturnCostDutyDiff() throws Exception {

        long date = new Date().getTime();
        FuelType fuelType = FuelType.DIESEL;
        double mpg = 2.1;
        double mileage = 122.5;
        URI uri = new URI(Service.calculator + "/calculate");
        logger.debug("uri = " + uri);
        CalcParams calcParams = new CalcParams(date, fuelType, mpg, mileage);
        RequestEntity<CalcParams> requestEntity = new RequestEntity<CalcParams>(calcParams, HttpMethod.GET, uri);

        ResponseEntity<Calculation> result = this.testRestTemplate.exchange(
                requestEntity, Calculation.class);

        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        Calculation calculation = result.getBody();
        assertThat(calculation.getCost()).isEqualTo(23.5);
        assertThat(calculation.getDuty()).isEqualTo(23.5);
        assertThat(calculation.getDiff()).isEqualTo(23.5);
    }

    @Test
    public void whenJavaSerializedToXmlStr_thenCorrect() throws JsonProcessingException {
        XmlMapper xmlMapper = new XmlMapper();
        String xml = xmlMapper.writeValueAsString(new CalcParams());
        assertThat(xml).isNotNull();
    }

    @Test
    public void whenJavaSerializedToXmlFile_thenCorrect() throws IOException {
        XmlMapper xmlMapper = new XmlMapper();
        xmlMapper.writeValue(new File("src/test/resources/CalcParams.xml"), new CalcParams());
        File file = new File("src/test/resources/CalcParams.xml");
        String absoutPath = file.getAbsolutePath();
        assertNotNull(file);
    }

}
