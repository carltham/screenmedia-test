/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.screenmedia.domain;

import uk.jeesoft.screenmedia.types.FuelType;

/**
 *
 * @author carl
 */
public class CalcParams {

    private long date;
    private FuelType fuelType;
    private double mpg;
    private double mileage;

    public CalcParams() {

    }

    public CalcParams(long date, FuelType fuelType, double mpg, double mileage) {
        this.date = date;
        this.fuelType = fuelType;
        this.mpg = mpg;
        this.mileage = mileage;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public double getMpg() {
        return mpg;
    }

    public void setMpg(double mpg) {
        this.mpg = mpg;
    }

    public double getMileage() {
        return mileage;
    }

    public void setMileage(double mileage) {
        this.mileage = mileage;
    }

}
