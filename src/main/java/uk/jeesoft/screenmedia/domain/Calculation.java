/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.screenmedia.domain;

/**
 *
 * @author carl
 */
public class Calculation {

    private double cost;
    private double duty;
    private double diff;

    private CalcParams calcParams;

    public Calculation() {

    }

    public Calculation(CalcParams calcParams) {
        this.calcParams = calcParams;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getDuty() {
        return duty;
    }

    public void setDuty(double duty) {
        this.duty = duty;
    }

    public double getDiff() {
        return diff;
    }

    public void setDiff(double diff) {
        this.diff = diff;
    }

}
