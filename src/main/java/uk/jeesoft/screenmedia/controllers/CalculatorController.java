package uk.jeesoft.screenmedia.controllers;

import java.util.concurrent.atomic.AtomicLong;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uk.jeesoft.screenmedia.domain.CalcParams;
import uk.jeesoft.screenmedia.domain.Calculation;
import uk.jeesoft.screenmedia.domain.Greeting;
import uk.jeesoft.screenmedia.types.Service;

@RestController
@RequestMapping(Service.calculator)
public class CalculatorController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping(path = "/hello-world", method = RequestMethod.GET)
    @Produces(MediaType.APPLICATION_JSON)
    public Greeting sayHello(@RequestParam(name = "name", required = false, defaultValue = "Stranger") String name) {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/calculate")
    public Calculation calculate(CalcParams calcParams) {
        return new Calculation(calcParams);
    }

}
