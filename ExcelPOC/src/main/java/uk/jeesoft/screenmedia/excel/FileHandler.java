/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.jeesoft.screenmedia.excel;

import java.io.File;
import java.net.URL;

/**
 *
 * @author carl
 */
public class FileHandler {
// get file from classpath, resources folder

    public static File getFileFromResources(String fileName) {

        ClassLoader classLoader = FileHandler.class.getClassLoader();

        URL resource = classLoader.getResource(fileName);
        if (resource == null) {
            throw new IllegalArgumentException("file is not found!");
        } else {
            return new File(resource.getFile());
        }

    }
}
